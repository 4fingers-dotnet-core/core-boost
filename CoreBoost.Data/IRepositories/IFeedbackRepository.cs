using CoreBoost.Data.Entities;
using CoreBoost.Infrastructure.Interfaces;

namespace CoreBoost.Data.IRepositories
{
    public interface IFeedbackRepository : IRepository<Feedback, int>
    {
    }
}