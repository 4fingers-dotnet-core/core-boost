using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Data.Enums
{
    public enum ArticleStatus
    {
        [Display(Name="Bị ẩn")]
        Hidden,
        [Display(Name="Nháp")]
        Draft,
        [Display(Name="Đã xuất bản")]
        Published
    }
}
