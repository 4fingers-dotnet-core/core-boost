﻿using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Data.Enums
{
    public enum Status
    {
        [Display(Name="Bị vô hiệu hoá")]
        Disabled,
        [Display(Name="Kích hoạt")]
        Active
    }
}
