﻿using CoreBoost.Data.Enums;
using CoreBoost.Data.Interfaces;
using CoreBoost.Infrastructure.SharedKernel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreBoost.Data.Entities
{
    public class Feedback : DomainEntity<int>, ISwitchable, IDateTracking
    {
        public Feedback() { }

        public Feedback(int id, string name, string email, string phone, string message, Status status)
        {
            Id = id;
            Name = name;
            Email = email;
            Phone = phone;
            Message = message;
            Status = status;
        }

        [StringLength(250)]
        [Required]
        public string Name { set; get; }

        [StringLength(250)]
        public string Email { set; get; }

        [StringLength(15)]
        public string Phone { set; get; }

        [StringLength(500)]
        public string Message { set; get; }

        public Status Status { set; get; }

        public DateTime DateCreated { set; get; }
        
        public DateTime DateModified { set; get; }
    }
}
