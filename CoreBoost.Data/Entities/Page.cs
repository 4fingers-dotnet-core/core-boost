﻿using CoreBoost.Data.Enums;
using CoreBoost.Data.Interfaces;
using CoreBoost.Infrastructure.SharedKernel;
using System.ComponentModel.DataAnnotations;
using System;

namespace CoreBoost.Data.Entities
{
    public class Page : DomainEntity<int>, ISwitchable, IDateTracking
    {
        public Page() { }

        public Page(string imageUrl, string slug, string title, string excerpt,
            string keywords, string content, Status status)
        {
            ImageUrl = imageUrl;
            Slug = slug;
            Title = title;
            Excerpt = excerpt;
            Keywords = keywords;
            Content = content;
            Status = status;
        }

        public Page(int id, string imageUrl, string slug, string title, string excerpt,
            string keywords, string content, Status status)
        {
            Id = id;
            ImageUrl = imageUrl;
            Slug = slug;
            Title = title;
            Excerpt = excerpt;
            Keywords = keywords;
            Content = content;
            Status = status;
        }

        public string ImageUrl { get; set; }

        [Required]
        [StringLength(100)]
        public string Slug { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public string Excerpt { get; set; }

        public string Keywords { get; set; }

        public string Content { get; set; }

        [Required]
        public Status Status { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set ; }
    }
}
