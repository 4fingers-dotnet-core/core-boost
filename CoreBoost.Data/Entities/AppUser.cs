﻿using CoreBoost.Data.Enums;
using CoreBoost.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreBoost.Data.Entities
{
    [Table("AppUsers")]
    public class AppUser : IdentityUser<Guid>, IDateTracking, ISwitchable
    {
        public AppUser() { }

        public AppUser(Guid id, string fullName, string userName,
            string email, string gender, string avatar, string role, Status status)
        {
            Id = id;
            FullName = fullName;
            UserName = userName;
            Email = email;
            Gender = gender;
            Avatar = avatar;
            Role = Role;
            Status = status;
        }
        
        public string FullName { get; set; }

        public string Gender { get; set; }

        public string Avatar { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public string Role { get; set; }

        public Status Status { get; set; }
    }
}
