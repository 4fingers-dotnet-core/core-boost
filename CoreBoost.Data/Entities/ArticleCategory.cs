using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoreBoost.Data.Enums;
using CoreBoost.Data.Interfaces;
using CoreBoost.Infrastructure.SharedKernel;

namespace CoreBoost.Data.Entities
{
    public class ArticleCategory : DomainEntity<int>, ISwitchable
    {
        public ArticleCategory() {}

        public ArticleCategory(string name, string description, Status status)
        {
            Name = name;
            Description = description;
            Status = status;
        }

        public ArticleCategory(int id, string name, string description, Status status)
        {
            Id = id;
            Name = name;
            Description = description;
            Status = status;
        }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public Status Status { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}