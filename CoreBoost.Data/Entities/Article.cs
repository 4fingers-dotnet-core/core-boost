using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreBoost.Data.Enums;
using CoreBoost.Data.Interfaces;
using CoreBoost.Infrastructure.SharedKernel;

namespace CoreBoost.Data.Entities
{
    public class Article : DomainEntity<int>, IDateTracking
    {
        public Article() {}

        public Article(int articleCategoryId, string imageUrl, string slug, string title,
            string excerpt, string keywords, string content, ArticleStatus status)
        {
            ArticleCategoryId = articleCategoryId;
            ImageUrl = imageUrl;
            Slug = slug;
            Title = title;
            Excerpt = excerpt;
            Keywords = keywords;
            Content = content;
            Status = status;
        }

        public Article(int id, int articleCategoryId, string imageUrl, string slug, string title,
            string excerpt, string keywords, string content, ArticleStatus status)
        {
            Id = id;
            ArticleCategoryId = articleCategoryId;
            ImageUrl = imageUrl;
            Slug = slug;
            Title = title;
            Excerpt = excerpt;
            Keywords = keywords;
            Content = content;
            Status = status;
        }
        public int ArticleCategoryId { get; set; }
        
        public string ImageUrl { get; set; }

        [Required]
        [StringLength(100)]
        public string Slug { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public string Excerpt { get; set; }

        public string Keywords { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
        
        public ArticleStatus Status { get; set; }

        [ForeignKey("ArticleCategoryId")]
        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}