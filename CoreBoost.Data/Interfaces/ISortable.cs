﻿namespace CoreBoost.Data.Interfaces
{
    public interface ISortable
    {
        int SortOrder { set; get; }
    }
}
