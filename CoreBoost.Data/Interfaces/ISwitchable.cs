﻿using CoreBoost.Data.Enums;

namespace CoreBoost.Data.Interfaces
{
    public interface ISwitchable
    {
        Status Status { set; get; }
    }
}
