﻿using CoreBoost.Data.Entities;
using CoreBoost.Data.Enums;
using CoreBoost.Utilities.Constants;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBoost.Data.EF
{
    public class DbInitializer
    {
        private readonly CoreBoostContext _context;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        public DbInitializer(CoreBoostContext context, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Initialize(CoreBoostContext context)
        {
            _context.Database.EnsureCreated();

            if (!_roleManager.Roles.Any())
            {
                await _roleManager.CreateAsync(new AppRole()
                {
                    Name = "Admin",
                    NormalizedName = "Admin",
                    Description = "Full accessed manager."
                });
                await _roleManager.CreateAsync(new AppRole()
                {
                    Name = "Writer",
                    NormalizedName = "Writer",
                    Description = "Content creator, can access articles and pages."
                });
            }

            if (!_userManager.Users.Any())
            {
                await _userManager.CreateAsync(new AppUser()
                {
                    UserName = "coreboost",
                    FullName = "Core Boost Administrator",
                    Role = "Admin",
                    Email = "coreboost@gmail.com",
                    Gender = "Male",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    Status = Status.Active
                }, "coreboost");
                var user = await _userManager.FindByNameAsync("coreboost");
                await _userManager.AddToRoleAsync(user, "Admin");
            }
            
            if (!_context.ArticleCategories.Any()) {
                var articleCategories = new List<ArticleCategory>{
                    new ArticleCategory() {Name="Category 1", Description="Category 1", Status=Status.Active},
                    new ArticleCategory() {Name="Category 2", Description="Category 2", Status=Status.Active},
                    new ArticleCategory() {Name="Category 3", Description="Category 3", Status=Status.Active},
                };
                _context.ArticleCategories.AddRange(articleCategories);
            }

            if (!_context.Articles.Any()) {
                var articles = new List<Article>{
                    new Article() {ArticleCategoryId=1, ImageUrl="", Slug="article-1", Title="Hello World Article", Excerpt="This is an initial content.", Keywords="hello, world", Content="<p>Hello world!</p>", Status=ArticleStatus.Published}
                };
                _context.Articles.AddRange(articles);
            }

            if (!_context.Pages.Any()) {
                var pages = new List<Page>{
                    new Page() {ImageUrl="none", Slug="page-1", Title="Hello World Page", Excerpt="This is an initial content.", Keywords="hello, world", Content="<p>Hello world!</p>", Status=Status.Active}
                };
                _context.Pages.AddRange(pages);
            }

            await _context.SaveChangesAsync();
        }
    }
}