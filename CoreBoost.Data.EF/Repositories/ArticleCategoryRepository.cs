using CoreBoost.Data.Entities;
using CoreBoost.Data.IRepositories;

namespace CoreBoost.Data.EF.Repositories
{
    public class ArticleCategoryRepository : EFRepository<ArticleCategory, int>, IArticleCategoryRepository
    {
        public ArticleCategoryRepository(CoreBoostContext context) : base(context)
        {
        }
    }
}