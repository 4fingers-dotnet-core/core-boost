using CoreBoost.Data.Entities;
using CoreBoost.Data.IRepositories;

namespace CoreBoost.Data.EF.Repositories
{
    public class ArticleRepository : EFRepository<Article, int>, IArticleRepository
    {
        public ArticleRepository(CoreBoostContext context) : base(context)
        {
        }
    }
}