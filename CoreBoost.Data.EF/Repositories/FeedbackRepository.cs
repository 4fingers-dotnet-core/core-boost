using CoreBoost.Data.Entities;
using CoreBoost.Data.IRepositories;

namespace CoreBoost.Data.EF.Repositories
{
    public class FeedbackRepository : EFRepository<Feedback, int>, IFeedbackRepository
    {
        public FeedbackRepository(CoreBoostContext context) : base(context)
        {
        }
    }
}