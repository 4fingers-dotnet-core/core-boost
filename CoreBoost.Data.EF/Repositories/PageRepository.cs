using CoreBoost.Data.Entities;
using CoreBoost.Data.IRepositories;

namespace CoreBoost.Data.EF.Repositories
{
    public class PageRepository : EFRepository<Page, int>, IPageRepository
    {
        public PageRepository(CoreBoostContext context) : base(context)
        {
        }
    }
}