﻿using CoreBoost.Infrastructure.Interfaces;

namespace CoreBoost.Data.EF
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly CoreBoostContext _context;
        public EFUnitOfWork(CoreBoostContext context)
        {
            _context = context;
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
