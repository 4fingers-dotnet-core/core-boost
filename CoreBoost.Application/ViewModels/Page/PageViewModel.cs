using System;
using System.ComponentModel.DataAnnotations;
using CoreBoost.Data.Enums;

namespace CoreBoost.Application.ViewModels.Page
{
    public class PageViewModel
    {
        public int Id { get; set; }

        [Display(Name="Đường Dẫn Ảnh")]
        public string ImageUrl { get; set; }

        [Required]
        [StringLength(100)]
        public string Slug { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Tiêu Đề")]
        public string Title { get; set; }

        [Display(Name="Mô Tả Ngắn")]
        public string Excerpt { get; set; }

        [Display(Name="Từ Khoá")]
        public string Keywords { get; set; }

        [Display(Name="Nội Dung")]
        public string Content { get; set; }

        [Required]
        public Status Status { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set ; }
    }
}