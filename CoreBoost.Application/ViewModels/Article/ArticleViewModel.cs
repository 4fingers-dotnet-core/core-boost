using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http.Internal;
using CoreBoost.Data.Entities;
using CoreBoost.Data.Enums;

namespace CoreBoost.Application.ViewModels.Article
{
    public class ArticleViewModel {
        public int Id { get; set; }

        public int ArticleCategoryId { get; set; }

        [Display(Name="Đường dẫn ảnh")]
        public string ImageUrl { get; set; }

        [Required]
        [StringLength(100)]
        public string Slug { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Tiêu đề")]
        public string Title { get; set; }

        [Display(Name="Mô tả ngắn")]
        public string Excerpt { get; set; }

        [Display(Name="Từ khoá")]        
        public string Keywords { get; set; }

        [Display(Name="Nội dung")]
        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
        
        public ArticleStatus Status { get; set; }

        public ArticleCategory ArticleCategory { get; set; }
    }
}