using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoreBoost.Data.Enums;

namespace CoreBoost.Application.ViewModels.Article
{
    public class ArticleCategoryViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Tên Danh Mục")]
        public string Name { get; set; }

        [StringLength(100)]
        [Display(Name="Mô Tả")]
        public string Description { get; set; }

        public Status Status { get; set; }
    }
}