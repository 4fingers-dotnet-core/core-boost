﻿using CoreBoost.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Application.ViewModels.System
{
    public class AppUserViewModel
    {
        
        public Guid? Id { set; get; }

        [Display(Name="Họ Tên")]
        public string FullName { set; get; }

        [Display(Name="Email")]
        public string Email { set; get; }

        [Display(Name="Mật khẩu")]
        public string Password { set; get; }

        [Display(Name="Tên Người Dùng")]
        public string UserName { set; get; }

        [Display(Name="Ảnh Đại Diện")]
        public string Avatar { get; set; }

        public Status Status { get; set; }

        [Display(Name="Giới Tính")]
        public string Gender { get; set; }

        public DateTime DateCreated { get; set; }

        public string Role { get; set; }
    }
}
