﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Application.ViewModels.System
{
    public class AppRoleViewModel
    {
        public Guid? Id { set; get; }

        [Display(Name="Tên Nhóm Quyền Hạn")]    
        public string Name { set; get; }

        [Display(Name="Mô Tả")]
        public string Description { set; get; }
    }
}
