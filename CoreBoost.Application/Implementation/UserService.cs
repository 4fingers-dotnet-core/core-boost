﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Data.Entities;
using CoreBoost.Utilities.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoreBoost.Application.Implementation
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMapper _mapper;

        public UserService(UserManager<AppUser> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<bool> AddAsync(AppUserViewModel userVm)
        {
            var user = new AppUser()
            {
                UserName = userVm.UserName,
                Avatar = userVm.Avatar,
                Gender = userVm.Gender,
                Email = userVm.Email,
                FullName = userVm.FullName,
                DateCreated = DateTime.Now,
                Status = userVm.Status
            };

            var result = await _userManager.CreateAsync(user, userVm.Password);

            if (result.Succeeded && !string.IsNullOrEmpty(userVm.Role))
            {
                var appUser = await _userManager.FindByNameAsync(user.UserName);

                if (appUser != null)
                    await _userManager.AddToRoleAsync(appUser, userVm.Role);
            }
            return true;
        }

        public async Task DeleteAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            await _userManager.DeleteAsync(user);
        }

        public async Task<List<AppUserViewModel>> GetAllAsync()
        {
            return await _userManager.Users.ProjectTo<AppUserViewModel>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<PagedResult<AppUserViewModel>> GetAllPagingAsync(string keyword, int? page, int? pageSize)
        {
            var query = _userManager.Users;

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.FullName.Contains(keyword)
                || x.UserName.Contains(keyword)
                || x.Email.Contains(keyword));

            int totalRow = query.Count();

            int pageValue = page.GetValueOrDefault(1);
            int pageSizeValue = page.GetValueOrDefault(10);

            query = query.Skip((pageValue - 1) * pageSizeValue)
               .Take(pageSizeValue);

            var data = query.Select(x => new AppUserViewModel()
            {
                UserName = x.UserName,
                Avatar = x.Avatar,
                Email = x.Email,
                FullName = x.FullName,
                Gender = x.Gender,
                Id = x.Id,
                Status = x.Status,
                DateCreated = x.DateCreated
            })
            .ToList();

            var paginationSet = new PagedResult<AppUserViewModel>()
            {
                Results = data,
                CurrentPage = pageValue,
                RowCount = totalRow,
                PageSize = pageSizeValue
            };

            return await Task.FromResult(paginationSet);
        }

        public async Task<AppUserViewModel> GetByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var roles = await _userManager.GetRolesAsync(user);
            var userVm = _mapper.Map<AppUser, AppUserViewModel>(user);

            //userVm.Roles = roles.ToList();

            return await Task.FromResult(userVm);
        }

        public async Task UpdateAsync(AppUserViewModel appUserViewModel)
        {
            var user = await _userManager.FindByIdAsync(appUserViewModel.Id.ToString());
            //Remove current roles in db
            var currentRoles = await _userManager.GetRolesAsync(user);
            var removeResult = await _userManager.RemoveFromRolesAsync(user, currentRoles);

            if (removeResult.Succeeded)
            {
                var result = _userManager.AddToRoleAsync(user, appUserViewModel.Role).Result.Succeeded;

                if (result) {
                    user.FullName = appUserViewModel.FullName;
                    user.Avatar = appUserViewModel.Avatar;
                    user.Status = appUserViewModel.Status;
                    user.Email = appUserViewModel.Email;
                    user.Gender = appUserViewModel.Gender;
                    
                    await _userManager.UpdateAsync(user);
                }
            }
        }
    }
}