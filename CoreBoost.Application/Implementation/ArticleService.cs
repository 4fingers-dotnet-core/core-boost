using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Data.Entities;
using CoreBoost.Data.Enums;
using CoreBoost.Infrastructure.Interfaces;
using CoreBoost.Utilities.Dtos;

namespace CoreBoost.Application.Implementation {
    public class ArticleService : IArticleService
    {
        private readonly IRepository<Article, int> _articleRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ArticleService(IRepository<Article, int> articleRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _articleRepository = articleRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public void Add(ArticleViewModel articleViewModel)
        {
            var article = _mapper.Map<ArticleViewModel, Article>(articleViewModel);
            _articleRepository.Add(article);
        }

        public void Delete(int id)
        {
            _articleRepository.Remove(id);
        }

        public void DeleteRange(List<int> idList)
        {
            foreach (var id in idList)
            {
                _articleRepository.Remove(id);
            }
        }

        public async Task<List<ArticleViewModel>> GetAllAsync() =>
            await _articleRepository
                .FindAll()
                .ProjectTo<ArticleViewModel>(_mapper.ConfigurationProvider)
                .ToListAsync();

        public async Task<PagedResult<ArticleViewModel>> GetAllPagingAsync(int? categoryId, string keyword, int? status, int? page, int? pageSize)
        {
            var query = _articleRepository.FindAll();

            int categoryIdValue = categoryId.GetValueOrDefault(-1);

            if (categoryIdValue != -1)
                query = query.Where(x => x.ArticleCategoryId == categoryIdValue);

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Title.Contains(keyword));

            int statusValue = status.GetValueOrDefault(-1);
            
            if (statusValue != -1)
                query = query.Where(x => x.Status == (ArticleStatus)statusValue);

            int pageValue = page.GetValueOrDefault(1);
            int pageSizeValue = pageSize.GetValueOrDefault(10);

            int totalRow = query.Count();

            query = query.OrderByDescending(x => x.DateCreated)
                .Skip((pageValue - 1) * pageSizeValue).Take(pageSizeValue);

            var data = await query.ProjectTo<ArticleViewModel>(_mapper.ConfigurationProvider).ToListAsync();

            var paginationSet = new PagedResult<ArticleViewModel>()
            {
                Results = data,
                CurrentPage = pageValue,
                RowCount = totalRow,
                PageSize = pageSizeValue,
            };
            
            return await Task.FromResult(paginationSet);
        }

        public async Task<List<ArticleViewModel>> GetLastestAsync(int number) =>
            await _articleRepository.FindAll()
                .OrderBy(x => x.DateCreated)
                .Take(number)
                .ProjectTo<ArticleViewModel>(_mapper.ConfigurationProvider)
                .ToListAsync();

        public async Task<ArticleViewModel> GetByIdAsync(int id) =>
            await Task.FromResult(
                _mapper.Map<Article, ArticleViewModel>(_articleRepository.FindById(id))
            );

        public async Task<ArticleViewModel> GetBySlugAsync(string slug) =>
            await Task.FromResult(
                _mapper.Map<Article, ArticleViewModel>(_articleRepository.FindSingle(a => a.Slug == slug))
            );

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ArticleViewModel articleViewModel)
        {
            _articleRepository.Update(_mapper.Map<ArticleViewModel, Article>(articleViewModel));
        }
    }
}