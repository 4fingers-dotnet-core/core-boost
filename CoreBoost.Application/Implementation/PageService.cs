﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CoreBoost.Application.Interfaces;
using CoreBoost.Data.Entities;
using CoreBoost.Infrastructure.Interfaces;
using CoreBoost.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreBoost.Application.ViewModels.Page;
using CoreBoost.Data.Enums;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CoreBoost.Application.Implementation
{
    public class PageService : IPageService
    {
        private readonly IRepository<Page, int> _pageRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PageService(IRepository<Page, int> pageRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _pageRepository = pageRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Add(PageViewModel pageViewModel)
        {
            var page = _mapper.Map<PageViewModel, Page>(pageViewModel);
            _pageRepository.Add(page);
        }

        public void Delete(int id)
        {
            _pageRepository.Remove(id);
        }

        public void DeleteRange(List<int> idList)
        {
            foreach (var id in idList)
            {
                _pageRepository.Remove(id);
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public async Task<List<PageViewModel>> GetAllAsync() =>
            await _pageRepository
                .FindAll()
                .ProjectTo<PageViewModel>(_mapper.ConfigurationProvider)
                .ToListAsync();

        public async Task<PagedResult<PageViewModel>> GetAllPagingAsync(string keyword, int? status, int? page, int? pageSize)
        {
            var query = _pageRepository.FindAll();

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Title.Contains(keyword));
            
            int statusValue = status.GetValueOrDefault(-1);

            if (statusValue != -1)
                query = query.Where(x => x.Status == (Status)statusValue);

            int pageValue = page.GetValueOrDefault(1);
            int pageSizeValue = pageSize.GetValueOrDefault(10);

            int totalRow = query.Count();

            query = query.OrderByDescending(x => x.DateModified)
                .Skip((pageValue - 1) * pageSizeValue).Take(pageSizeValue);

            var data = query.ProjectTo<PageViewModel>(_mapper.ConfigurationProvider).ToList();

            var paginationSet = new PagedResult<PageViewModel>()
            {
                Results = data,
                CurrentPage = pageValue,
                RowCount = totalRow,
                PageSize = pageSizeValue,
            };

            return await Task.FromResult(paginationSet);
        }

        public async Task<PageViewModel> GetByIdAsync(int id) =>
            await Task.FromResult(
                _mapper.Map<Page, PageViewModel>(_pageRepository.FindById(id))
            );

        public async Task<PageViewModel> GetBySlugAsync(string slug) =>
            await Task.FromResult(
                _mapper.Map<Page, PageViewModel>(_pageRepository.FindSingle(a => a.Slug == slug))
            );

        public async Task<List<PageViewModel>> GetLastestAsync(int number) =>
            await _pageRepository.FindAll()
                .OrderBy(x => x.DateCreated)
                .Take(number)
                .ProjectTo<PageViewModel>(_mapper.ConfigurationProvider)
                .ToListAsync();

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PageViewModel pageViewModel)
        {
            _pageRepository.Update(_mapper.Map<PageViewModel, Page>(pageViewModel));
        }
    }
}