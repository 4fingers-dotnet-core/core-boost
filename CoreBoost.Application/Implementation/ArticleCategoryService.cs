using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Data.Entities;
using CoreBoost.Data.Enums;
using CoreBoost.Infrastructure.Interfaces;
using CoreBoost.Utilities.Dtos;

namespace CoreBoost.Application.Implementation {
    public class ArticleCategoryService : IArticleCategoryService
    {
        private readonly IRepository<ArticleCategory, int> _articleCategoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ArticleCategoryService(IRepository<ArticleCategory, int> articleCategoryRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _articleCategoryRepository = articleCategoryRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Add(ArticleCategoryViewModel articleCategoryViewModel)
        {
            var article = _mapper.Map<ArticleCategoryViewModel, ArticleCategory>(articleCategoryViewModel);
            _articleCategoryRepository.Add(article);
        }

        public void Delete(int id)
        {
            _articleCategoryRepository.Remove(id);
        }

        public void DeleteRange(List<int> idList)
        {
            foreach (var id in idList)
            {
                _articleCategoryRepository.Remove(id);
            }
        }

        public async Task<List<ArticleCategoryViewModel>> GetAllAsync() => await _articleCategoryRepository.FindAll()
            .ProjectTo<ArticleCategoryViewModel>(_mapper.ConfigurationProvider).ToListAsync();

        public async Task<PagedResult<ArticleCategoryViewModel>> GetAllPagingAsync(string keyword, int? status, int? page, int? pageSize)
        {
            var query = _articleCategoryRepository.FindAll();

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword));

            int statusValue = status.GetValueOrDefault(-1);
            if (statusValue != -1)
                query = query.Where(x => x.Status == (Status)statusValue);

            int pageValue = page.GetValueOrDefault(1);
            int pageSizeValue = pageSize.GetValueOrDefault(10);

            int totalRow = query.Count();

            query = query.OrderBy(x => x.Name)
                .Skip((pageValue - 1) * pageSizeValue).Take(pageSizeValue);

            var data = await query.ProjectTo<ArticleCategoryViewModel>(_mapper.ConfigurationProvider).ToListAsync();

            var paginationSet = new PagedResult<ArticleCategoryViewModel>()
            {
                Results = data,
                CurrentPage = pageValue,
                RowCount = totalRow,
                PageSize = pageSizeValue,
            };
            return await Task.FromResult(paginationSet);
        }

        public async Task<ArticleCategoryViewModel> GetByIdAsync(int id) =>
            await Task.FromResult(
                _mapper.Map<ArticleCategory, ArticleCategoryViewModel>(_articleCategoryRepository.FindById(id))
            );

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ArticleCategoryViewModel articleCategoryViewModel)
        {
            _articleCategoryRepository.Update(_mapper.Map<ArticleCategoryViewModel, ArticleCategory>(articleCategoryViewModel));
        }
    }
}