﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Data.Entities;
using CoreBoost.Infrastructure.Interfaces;
using CoreBoost.Utilities.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBoost.Application.Implementation
{
    public class RoleService : IRoleService
    {
        private RoleManager<AppRole> _roleManager;
        private IRepository<Function, string> _functionRepository;
        private IRepository<Permission, int> _permissionRepository;
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RoleService(RoleManager<AppRole> roleManager, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> AddAsync(AppRoleViewModel appRoleViewModel)
        {
            var roleCheck = await _roleManager.RoleExistsAsync(appRoleViewModel.Name); 

            if (!roleCheck) {
                var role = new AppRole()
                {
                    Name = appRoleViewModel.Name,
                    Description = appRoleViewModel.Description
                };
                var result = await _roleManager.CreateAsync(role);

                _unitOfWork.Commit();

                return result.Succeeded;
            }
            else
                return false;
        }

        public Task<bool> CheckPermission(string functionId, string action, string[] roles)
        {
            var functions = _functionRepository.FindAll();
            var permissions = _permissionRepository.FindAll();
            var query = from f in functions
                        join p in permissions on f.Id equals p.FunctionId
                        join r in _roleManager.Roles on p.RoleId equals r.Id
                        where roles.Contains(r.Name) && f.Id == functionId
                        && ((p.CanCreate && action == "Create")
                        || (p.CanUpdate && action == "Update")
                        || (p.CanDelete && action == "Delete")
                        || (p.CanRead && action == "Read"))
                        select p;
            return query.AnyAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());
            await _roleManager.DeleteAsync(role);
        }

        public async Task<List<AppRoleViewModel>> GetAllAsync()
        {
            return await _roleManager.Roles.ProjectTo<AppRoleViewModel>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<PagedResult<AppRoleViewModel>> GetAllPagingAsync(string keyword, int? page, int? pageSize)
        {
            var query = _roleManager.Roles;

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword)
                || x.Description.Contains(keyword));

            int totalRow = query.Count();

            int pageValue = page.GetValueOrDefault(1);
            int pageSizeValue = pageSize.GetValueOrDefault(10);

            query = query.Skip((pageValue - 1) * pageSizeValue)
               .Take(pageSizeValue);

            var data = query.ProjectTo<AppRoleViewModel>(_mapper.ConfigurationProvider).ToList();
            var paginationSet = new PagedResult<AppRoleViewModel>()
            {
                Results = data,
                CurrentPage = pageValue,
                RowCount = totalRow,
                PageSize = pageSizeValue
            };

            return await Task.FromResult(paginationSet);
        }

        public async Task<AppRoleViewModel> GetByIdAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());

            return _mapper.Map<AppRole, AppRoleViewModel>(role);
        }

        public List<PermissionViewModel> GetListFunctionWithRole(Guid roleId)
        {
            var functions = _functionRepository.FindAll();
            var permissions = _permissionRepository.FindAll();

            var query = from f in functions
                        join p in permissions on f.Id equals p.FunctionId into fp
                        from p in fp.DefaultIfEmpty()
                        where p != null && p.RoleId == roleId
                        select new PermissionViewModel()
                        {
                            RoleId = roleId,
                            FunctionId = f.Id,
                            CanCreate = p != null ? p.CanCreate : false,
                            CanDelete = p != null ? p.CanDelete : false,
                            CanRead = p != null ? p.CanRead : false,
                            CanUpdate = p != null ? p.CanUpdate : false
                        };
            return query.ToList();
        }

        public void SavePermission(List<PermissionViewModel> permissionVms, Guid roleId)
        {
            var permissions = _mapper.Map<List<PermissionViewModel>, List<Permission>>(permissionVms);
            var oldPermission = _permissionRepository.FindAll().Where(x => x.RoleId == roleId).ToList();
            if (oldPermission.Count > 0)
            {
                _permissionRepository.RemoveMultiple(oldPermission);
            }
            foreach (var permission in permissions)
            {
                _permissionRepository.Add(permission);
            }
            _unitOfWork.Commit();
        }

        public async Task<bool> UpdateAsync(AppRoleViewModel roleVm)
        {
            var role = await _roleManager.FindByIdAsync(roleVm.Id.ToString());
            role.Description = roleVm.Description;
            role.Name = roleVm.Name;

            var result = _roleManager.UpdateAsync(role).Result.Succeeded;
            return result;
        }
    }
}