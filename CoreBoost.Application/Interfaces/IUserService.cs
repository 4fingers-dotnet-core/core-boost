﻿using CoreBoost.Application.ViewModels.System;
using CoreBoost.Utilities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreBoost.Application.Interfaces
{
    public interface IUserService
    {
        Task<bool> AddAsync(AppUserViewModel userVm);

        Task DeleteAsync(string id);

        Task<List<AppUserViewModel>> GetAllAsync();

        Task<PagedResult<AppUserViewModel>> GetAllPagingAsync(string keyword, int? page, int? pageSize);

        Task<AppUserViewModel> GetByIdAsync(string id);

        Task UpdateAsync(AppUserViewModel userVm);
    }
}
