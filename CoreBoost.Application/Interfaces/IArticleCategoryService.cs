using System.Collections.Generic;
using System.Threading.Tasks;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Utilities.Dtos;

namespace CoreBoost.Application.Interfaces {
    public interface IArticleCategoryService
    {
        // CRUD
        void Add(ArticleCategoryViewModel articleCategoryViewModel);

        void Update(ArticleCategoryViewModel articleCategoryViewModel);

        void Delete(int id);

        void DeleteRange(List<int> idList);

        // Gets
        Task<List<ArticleCategoryViewModel>> GetAllAsync();

        Task<PagedResult<ArticleCategoryViewModel>> GetAllPagingAsync(string keyword, int? status, int? page, int? pageSize);

        Task<ArticleCategoryViewModel> GetByIdAsync(int id);

        void Save();
    }
}