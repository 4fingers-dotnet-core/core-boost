﻿using CoreBoost.Utilities.Dtos;
using System;
using System.Collections.Generic;
using CoreBoost.Application.ViewModels.Page;
using System.Threading.Tasks;

namespace CoreBoost.Application.Interfaces
{
    public interface IPageService : IDisposable
    {
        void Add(PageViewModel pageViewModel);

        void Update(PageViewModel pageViewModel);

        void Delete(int id);

        void DeleteRange(List<int> idList);
        
        Task<List<PageViewModel>> GetAllAsync();

        Task<PagedResult<PageViewModel>> GetAllPagingAsync(string keyword, int? status, int? page, int? pageSize);

        Task<List<PageViewModel>> GetLastestAsync(int number);

        Task<PageViewModel> GetByIdAsync(int id);

        Task<PageViewModel> GetBySlugAsync(string slug);

        void Save();

    }
}
