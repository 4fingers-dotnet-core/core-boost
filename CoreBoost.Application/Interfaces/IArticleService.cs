using System.Collections.Generic;
using System.Threading.Tasks;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Utilities.Dtos;

namespace CoreBoost.Application.Interfaces {
    public interface IArticleService
    {
        // CRUD
        void Add(ArticleViewModel articleViewModel);

        void Update(ArticleViewModel articleViewModel);

        void Delete(int id);

        void DeleteRange(List<int> idList);

        // Gets
        Task<List<ArticleViewModel>> GetAllAsync();

        Task<PagedResult<ArticleViewModel>> GetAllPagingAsync(int? categoryId, string keyword, int? status, int? page, int? pageSize);

        Task<List<ArticleViewModel>> GetLastestAsync(int number);

        Task<ArticleViewModel> GetByIdAsync(int id);

        Task<ArticleViewModel> GetBySlugAsync(string slug);

        void Save();
    }
}