﻿using CoreBoost.Application.ViewModels.System;
using CoreBoost.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreBoost.Application.Interfaces
{
    public interface IRoleService
    {
        Task<bool> AddAsync(AppRoleViewModel appRoleViewModel);

        Task DeleteAsync(Guid id);

        Task<List<AppRoleViewModel>> GetAllAsync();

        Task<PagedResult<AppRoleViewModel>> GetAllPagingAsync(string keyword, int? page, int? pageSize);

        Task<AppRoleViewModel> GetByIdAsync(Guid id);

        Task<bool> UpdateAsync(AppRoleViewModel userVm);

        //List<PermissionViewModel> GetListFunctionWithRole(Guid roleId);

        //void SavePermission(List<PermissionViewModel> permissions, Guid roleId);

        Task<bool> CheckPermission(string functionId, string action, string[] roles);
    }
}
