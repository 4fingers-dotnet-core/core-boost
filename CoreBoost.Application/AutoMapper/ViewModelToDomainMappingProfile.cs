﻿using AutoMapper;
using CoreBoost.Application.ViewModels.Common;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Data.Entities;
using System;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Application.ViewModels.Page;

namespace CoreBoost.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<AppUserViewModel, AppUser>().ConstructUsing(c => new AppUser(
                c.Id.GetValueOrDefault(Guid.Empty), c.FullName, c.UserName, c.Email, c.Gender,
                    c.Avatar, c.Role, c.Status
            ));

            // CreateMap<PermissionViewModel, Permission>().ConstructUsing(c => new Permission(
            //     c.RoleId, c.FunctionId, c.CanCreate, c.CanRead, c.CanUpdate, c.CanDelete
            // ));

            CreateMap<ContactViewModel, Contact>().ConstructUsing(c => new Contact(
                c.Id, c.Name, c.Phone, c.Email, c.Website, c.Address, c.Other, c.Lng,
                c.Lat, c.Status
            ));

            CreateMap<FeedbackViewModel, Feedback>().ConstructUsing(c => new Feedback(
                c.Id, c.Name, c.Email, c.Phone, c.Message, c.Status
            ));

            // Custom for CoreBoost
            CreateMap<ArticleViewModel, Article>().ConstructUsing(c => new Article(
                c.Id, c.ImageUrl, c.Slug, c.Title, c.Excerpt, c.Keywords, c.Content, c.Status
            ));

            CreateMap<ArticleCategoryViewModel, ArticleCategory>().ConstructUsing(c => new ArticleCategory(
               c.Id, c.Name, c.Description, c.Status
            ));
            
            CreateMap<PageViewModel, Page>().ConstructUsing(c => new Page(
                c.Id, c.ImageUrl, c.Slug, c.Title, c.Excerpt, c.Keywords, c.Content, c.Status
            ));
        }
    }
}