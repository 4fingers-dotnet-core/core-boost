﻿using AutoMapper;
using CoreBoost.Application.ViewModels.Common;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Data.Entities;
using CoreBoost.Application.ViewModels.Page;

namespace CoreBoost.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<AppUser, AppUserViewModel>();
            CreateMap<AppRole, AppRoleViewModel>();

            CreateMap<Feedback, FeedbackViewModel>().MaxDepth(2);
            CreateMap<Contact, ContactViewModel>().MaxDepth(2);
            
            // VEnthusiam
            CreateMap<Article, ArticleViewModel>();
            CreateMap<ArticleCategory, ArticleCategoryViewModel>();
            
            CreateMap<Page, PageViewModel>();

            
        }
    }
}