# CoreBoost

[CoreBoost](https://CoreBoost.com) website, which built on .NET Core library.

## Codebase

- Based on [**teducoreapp**](https://github.com/teduinternational/teducoreapp)
- Current .NET Core version: 3.1

## Architecture:

- Domain Driven Design (DDD)
- 5 layers

## Projects in solution explained:

- CoreBoost-client-app: Vue.js components
- CoreBoost: main project
- CoreBoost.Application: 
- CoreBoost.Application.Dapper: 
- CoreBoost.Data: EF enties
- CoreBoost.Data.EF: EF configs
- CoreBoost.Infrastructure: 
- CoreBoost.Utilities:
- CoreBoost.WebApi: APIs

## Deploy target

- Windows Server
- Lastest IIS
- Lastest SQLServer Express

## License

Not determined yet

Made with all enthusiasm by **4fingers**.