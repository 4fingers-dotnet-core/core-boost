using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Areas.Admin.Controllers;
using CoreBoost.Data.Enums;
using CoreBoost.Data.IRepositories;
using CoreBoost.Models.ArticleViewModels;

namespace VEenthusiam.Areas.Admin.Controllers
{
    public class ArticleCategoryController : BaseController
    {
        private IArticleCategoryService _articleCategoryService;
        private IArticleCategoryRepository _articleCategoryRepository;

        public ArticleCategoryController(IArticleCategoryService articleCategoryService, IArticleCategoryRepository articleCategoryRepository)
        {
            _articleCategoryService = articleCategoryService;
            _articleCategoryRepository = articleCategoryRepository;
        }

        public async Task<IActionResult> Browse(string keyword, int? status, int? page, int? pageSize)
        {
            var fanpages = await _articleCategoryService.GetAllPagingAsync(keyword, status, page, pageSize);
            
            ViewBag.AllCount = await _articleCategoryRepository.FindAll().CountAsync();
            ViewBag.ActiveCount = await _articleCategoryRepository.FindAll(ac => ac.Status == Status.Active).CountAsync();
            ViewBag.InActiveCount = await _articleCategoryRepository.FindAll(ac => ac.Status == Status.Disabled).CountAsync();

            return View(fanpages);
        }

        public IActionResult Add()
        {
            ViewData["Title"] = "Thêm Danh Mục Bài Viết";
            return View();
        }

        [HttpPost]
        public IActionResult Add(ArticleCategoryViewModel articleCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleCategoryService.Add(articleCategoryViewModel);
                _articleCategoryService.Save();
                return Redirect("/admin/articlecategory/browse");
            }
        }

        [HttpPost]
        public IActionResult AddAndContinue(ArticleCategoryViewModel articleCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleCategoryService.Add(articleCategoryViewModel);
                _articleCategoryService.Save();
                return Redirect("/admin/articlecategory/add");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _articleCategoryService.GetByIdAsync(id);

            ViewData["Title"] = "Chỉnh Sửa Bài Viết";
            ViewBag.Id = id;

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(ArticleCategoryViewModel articleCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleCategoryService.Update(articleCategoryViewModel);
                _articleCategoryService.Save();
                return Redirect("/admin/articlecategory/browse");
            }
        }

        #region AJAX API

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _articleCategoryService.GetAllAsync();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPaging(string keyword, int? status, int? page, int? pageSize)
        {
            var model = await _articleCategoryService.GetAllPagingAsync(keyword, status, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStatusCount() 
        {
            ArticleCountViewModel articleCount = new ArticleCountViewModel();
            articleCount.All = await _articleCategoryRepository.FindAll().CountAsync();
            articleCount.Active = await _articleCategoryRepository.FindAll(a => a.Status == Status.Active).CountAsync();
            articleCount.InActive = await _articleCategoryRepository.FindAll(a => a.Status == Status.Disabled).CountAsync();

            return new OkObjectResult(articleCount);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            var model = await _articleCategoryService.GetByIdAsync(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult SaveEntity(ArticleCategoryViewModel articleCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (articleCategoryViewModel.Id == 0)
                {
                    _articleCategoryService.Add(articleCategoryViewModel);
                }
                else
                {
                    _articleCategoryService.Update(articleCategoryViewModel);
                }
                _articleCategoryService.Save();
                return new OkObjectResult(articleCategoryViewModel);
            }
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _articleCategoryService.Delete(id);
                _articleCategoryService.Save();
                return new OkObjectResult(id);
            }
        }

        [HttpPost]
        public IActionResult DeleteRange(List<int> idList)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else if (idList.Count == 0)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _articleCategoryService.DeleteRange(idList);
                _articleCategoryService.Save();
                return new OkObjectResult(idList);
            }
        }

        #endregion AJAX API
    }
}