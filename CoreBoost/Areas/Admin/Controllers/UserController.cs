﻿using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Authorization;
using CoreBoost.Data.Enums;
using CoreBoost.Extensions;
using CoreBoost.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreBoost.Data.EF;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CoreBoost.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private CoreBoostContext _context;
        private readonly IAuthorizationService _authorizationService;

        public UserController(IUserService userService, CoreBoostContext context, IAuthorizationService authorizationService)
        {
            _userService = userService;
            _context = context;
            _authorizationService = authorizationService;
        }

        public async Task<IActionResult> Browse(string keyword, int? page, int? pageSize)
        {
            var user = await _userService.GetAllPagingAsync(keyword, page, pageSize);
            
            ViewBag.AllCount = _userService.GetAllAsync().Result.Count();
            ViewBag.ActiveCount = await _context.AppUsers.Select(u => u.Status == Status.Active).CountAsync();
            ViewBag.InActiveCount = await _context.AppUsers.Select(u => u.Status == Status.Disabled).CountAsync();

            return View(user);
        }

        public async Task<IActionResult> Add()
        {
            ViewData["Title"] = "Thêm Người Dùng";

            ViewBag.Roles = await _context.Roles.Select(r => new SelectListItem() {
                Value = r.Name,
                Text = r.Name
            })
            .ToListAsync();

            ViewBag.Genders = new List<SelectListItem>() {
                new SelectListItem("Nam", "Male"),
                new SelectListItem("Nữ", "Female")
            };

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(AppUserViewModel appUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                await _userService.AddAsync(appUserViewModel);
                return RedirectPermanent("/admin/user/browse");
            }
        }

        [HttpPost]
        public IActionResult AddAndContinue(AppUserViewModel appUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _userService.AddAsync(appUserViewModel);
                return Redirect("/admin/user/add");
            }
        }

        public async Task<IActionResult> Edit(string id)
        {
            var model = await _userService.GetByIdAsync(id);

            ViewData["Title"] = "Chỉnh Sửa Thông Tin Người Dùng";

            ViewBag.Roles = await _context.Roles.Select(r => new SelectListItem() {
                Value = r.Name,
                Text = r.Name
            })
            .ToListAsync();

            ViewBag.Genders = new List<SelectListItem>() {
                new SelectListItem("Nam", "Male"),
                new SelectListItem("Nữ", "Female")
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(AppUserViewModel articleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                await _userService.UpdateAsync(articleViewModel);
                return Redirect("/admin/user/browse");
            }
        }

        public async Task<IActionResult> Index()
        {
            var result = await _authorizationService.AuthorizeAsync(User, "USER", Operations.Read);
            if (result.Succeeded == false)
                return new RedirectResult("/Admin/Login/Index");

            return View();
        }

        public async Task<IActionResult> GetAll()
        {
            var model = await _userService.GetAllAsync();

            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(string id)
        {
            var model = await _userService.GetByIdAsync(id);

            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var model = _userService.GetAllPagingAsync(keyword, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveEntity(AppUserViewModel userVm)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            if (userVm.Id == null)
            {
                // var announcement = new AnnouncementViewModel()
                // {
                //     Content = $"User {userVm.UserName} has been created",
                //     DateCreated = DateTime.Now,
                //     Status = Status.Active,
                //     Title = "User created",
                //     UserId = User.GetUserId(),
                //     Id = Guid.NewGuid().ToString(),
                // };
                await _userService.AddAsync(userVm);
            }
            else
            {
                await _userService.UpdateAsync(userVm);
            }
            return new OkObjectResult(userVm);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                await _userService.DeleteAsync(id);

                return new OkObjectResult(id);
            }
        }
    }
}