﻿using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.System;
using CoreBoost.Extensions;
using CoreBoost.SignalR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBoost.Areas.Admin.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        public async Task<IActionResult> Browse(string keyword, int? page, int? pageSize)
        {
            var model = await _roleService.GetAllPagingAsync(keyword, page, pageSize);

            return View(model);
        }

        public IActionResult Add()
        {
            ViewData["Title"] = "Thêm Quyền Hạn";

            return View();
        }

        [HttpPost]
        public IActionResult Add(AppRoleViewModel appRoleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                var result = _roleService.AddAsync(appRoleViewModel).Result;

                if (result)
                    return RedirectPermanent("/admin/role/browse");
                else
                    return new BadRequestObjectResult(ModelState);
            }
        }

        public async Task<IActionResult> Edit(string id)
        {
            var model = await _roleService.GetByIdAsync(new Guid(id));

            ViewData["Title"] = "Chỉnh Sửa Quyền Hạn";

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AppRoleViewModel appRoleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                var result = _roleService.UpdateAsync(appRoleViewModel).Result;

                if (result)
                    return Redirect("/admin/role/browse");
                else
                    return new BadRequestObjectResult(ModelState);
            }
        }

        public async Task<IActionResult> GetAll()
        {
            var model = await _roleService.GetAllAsync();

            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            await _roleService.DeleteAsync(id);

            return new OkObjectResult(id);
        }

        // [HttpPost]
        // public IActionResult ListAllFunction(Guid roleId)
        // {
        //     var functions = _roleService.GetListFunctionWithRole(roleId);
        //     return new OkObjectResult(functions);
        // }

        // [HttpPost]
        // public IActionResult SavePermission(List<PermissionViewModel> listPermmission, Guid roleId)
        // {
        //     _roleService.SavePermission(listPermmission, roleId);
        //     return new OkResult();
        // }
    }
}