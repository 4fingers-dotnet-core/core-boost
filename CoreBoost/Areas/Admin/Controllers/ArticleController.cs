using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoreBoost.Application.Interfaces;
using CoreBoost.Application.ViewModels.Article;
using CoreBoost.Data.EF;
using CoreBoost.Data.Enums;
using CoreBoost.Data.IRepositories;
using CoreBoost.Models;
using CoreBoost.Models.ArticleViewModels;

namespace CoreBoost.Areas.Admin.Controllers
{
    public class ArticleController : BaseController
    {
        private IArticleService _articleService;
        private IArticleRepository _articleRepository;

        public CoreBoostContext _context;

        public ArticleController(IArticleService articleService, IArticleRepository articleRepository, CoreBoostContext context)
        {
            _articleService = articleService;
            _articleRepository = articleRepository;
            _context = context;
        }

        public async Task<IActionResult> Browse(int? categoryId, string keyword, int? status, int? page, int? pageSize)
        {
            var fanpages = await _articleService.GetAllPagingAsync(categoryId, keyword, status, page, pageSize);
            
            ViewBag.AllCount = await _articleRepository.FindAll().CountAsync();
            ViewBag.ActiveCount = await _articleRepository.FindAll(f => f.Status == ArticleStatus.Published).CountAsync();
            ViewBag.InActiveCount = await _articleRepository.FindAll(
                f => f.Status == ArticleStatus.Draft && f.Status == ArticleStatus.Hidden
            ).CountAsync();

            return View(fanpages);
        }

        public async Task<IActionResult> Add()
        {
            ViewData["Title"] = "Thêm Bài Viết";

            ViewBag.ArticleCategories = await _context.ArticleCategories.Select(ac => new SelectListItem() {
                Value = ac.Id.ToString(),
                Text = ac.Name
            })
            .ToListAsync();
            return View();
        }

        [HttpPost]
        public IActionResult Add(ArticleViewModel articleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleService.Add(articleViewModel);
                _articleService.Save();
                return RedirectPermanent("/admin/article/browse");
            }
        }

        [HttpPost]
        public IActionResult AddAndContinue(ArticleViewModel articleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleService.Add(articleViewModel);
                _articleService.Save();
                return Redirect("/admin/article/add");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _articleService.GetByIdAsync(id);

            ViewData["Title"] = "Chỉnh Sửa Bài Viết";
            ViewBag.ArticleCategories = await _context.ArticleCategories.Select(ac => new SelectListItem() {
                Value = ac.Id.ToString(),
                Text = ac.Name
            })
            .ToListAsync();

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(ArticleViewModel articleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _articleService.Update(articleViewModel);
                _articleService.Save();
                return Redirect("/admin/article/browse");
            }
        }

        #region AJAX API

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _articleService.GetAllAsync();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPaging(int? categoryId, string keyword, int? status, int? page, int? pageSize)
        {
            var model = await _articleService.GetAllPagingAsync(categoryId, keyword, status, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStatusCount() 
        {
            ArticleCountViewModel articleCount = new ArticleCountViewModel();
            articleCount.All = await _articleRepository.FindAll().CountAsync();
            articleCount.Active = await _articleRepository.FindAll(a => a.Status == ArticleStatus.Published).CountAsync();
            articleCount.InActive = await _articleRepository.FindAll(
                a => a.Status == ArticleStatus.Draft && a.Status == ArticleStatus.Hidden
            ).CountAsync();

            return new OkObjectResult(articleCount);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            var model = await _articleService.GetByIdAsync(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult SaveEntity(ArticleViewModel articleViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (articleViewModel.Id == 0)
                {
                    _articleService.Add(articleViewModel);
                }
                else
                {
                    _articleService.Update(articleViewModel);
                }
                _articleService.Save();
                return new OkObjectResult(articleViewModel);
            }
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _articleService.Delete(id);
                _articleService.Save();
                return new OkObjectResult(id);
            }
        }

        [HttpPost]
        public IActionResult DeleteRange(List<int> idList)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else if (idList.Count == 0)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _articleService.DeleteRange(idList);
                _articleService.Save();
                return new OkObjectResult(idList);
            }
        }

        #endregion AJAX API
    }
}