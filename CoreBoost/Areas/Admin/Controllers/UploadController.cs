﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreBoost.Areas.Admin.Controllers
{
    public class UploadController : BaseController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public UploadController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult UploadSummernoteImage()
        {
            string[] _permittedImageExtensions = { ".jpg", ".jpeg", ".png" };
            var files = Request.Form.Files;
            var type = Request.Form["type"];
            var slug = Request.Form["slug"];

            if (files.Count == 0)
            {
                return new BadRequestObjectResult(files);
            }
            else
            {
                var file = files[0];
                var fileName = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');
                var fileBaseName = Path.GetFileNameWithoutExtension(fileName);
                var fileExtension = Path.GetExtension(fileName).ToLowerInvariant();

                if (string.IsNullOrEmpty(fileExtension) || !_permittedImageExtensions.Contains(fileExtension)) {
                    return new BadRequestObjectResult("Định dạng file không phù hợp!");
                }

                string folder = Path.Combine(_hostingEnvironment.WebRootPath, "img", "uploaded", type);

                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                int fileNumber = 1;
                var newFileName = $"{slug}_{fileNumber}{fileExtension}";

                while (System.IO.File.Exists(Path.Combine(folder, newFileName)))
                {
                    fileNumber++;
                    newFileName = $"{slug}_{fileNumber}{fileExtension}";
                }

                string filePath = Path.Combine(folder, newFileName);

                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                
                return new OkObjectResult($"/img/uploaded/{type}/{newFileName}");
            }
        }
        /// <summary>
        /// Upload image for form
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public IActionResult UploadThumbnail()
        {
            string[] _permittedImageExtensions = { ".jpg", ".jpeg", ".png" };
            var files = Request.Form.Files;
            var type = Request.Form["type"];
            var slug = Request.Form["slug"];

            if (files.Count == 0)
            {
                return new BadRequestObjectResult(files);
            }
            else
            {
                var file = files[0];
                var fileName = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');
                var fileBaseName = Path.GetFileNameWithoutExtension(fileName);
                var fileExtension = Path.GetExtension(fileName).ToLowerInvariant();

                if (string.IsNullOrEmpty(fileExtension) || !_permittedImageExtensions.Contains(fileExtension)) {
                    return new BadRequestObjectResult("Định dạng file không phù hợp!");
                }

                string folder = Path.Combine(_hostingEnvironment.WebRootPath, "img", "uploaded", type);

                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                var newFileName = $"{slug}_thumbnail{fileExtension}";
                string filePath = Path.Combine(folder, newFileName);

                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                
                return new OkObjectResult($"/img/uploaded/{type}/{newFileName}");
            }
        }
    }
}
