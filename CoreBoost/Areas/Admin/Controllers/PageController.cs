﻿using CoreBoost.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;
using CoreBoost.Application.ViewModels.Page;
using System.Threading.Tasks;
using CoreBoost.Data.IRepositories;
using Microsoft.EntityFrameworkCore;
using CoreBoost.Data.Enums;

namespace CoreBoost.Areas.Admin.Controllers
{
    public class PageController : BaseController
    {
        private IPageService _pageService;
        private IPageRepository _pageRepository;

        public PageController(IPageService pageService, IPageRepository pageRepository)
        {
            _pageService = pageService;
            _pageRepository = pageRepository;
        }

        public async Task<IActionResult> Browse(string keyword, int? status, int? page, int? pageSize)
        {
            var pages = await _pageService.GetAllPagingAsync(keyword, status, page, pageSize);
            
            ViewBag.AllCount = await _pageRepository.FindAll().CountAsync();
            ViewBag.ActiveCount = await _pageRepository.FindAll(ac => ac.Status == Status.Active).CountAsync();
            ViewBag.InActiveCount = await _pageRepository.FindAll(ac => ac.Status == Status.Disabled).CountAsync();

            return View(pages);
        }

        public IActionResult Add()
        {
            ViewData["Title"] = "Thêm Trang";

            return View();
        }

        [HttpPost]
        public IActionResult Add(PageViewModel pageViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _pageService.Add(pageViewModel);
                _pageService.Save();
                return Redirect("/admin/page/browse");
            }
        }

        [HttpPost]
        public IActionResult AddAndContinue(PageViewModel pageViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _pageService.Add(pageViewModel);
                _pageService.Save();
                return Redirect("/admin/page/add");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _pageService.GetByIdAsync(id);

            ViewData["Title"] = "Chỉnh Sửa Trang";

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PageViewModel pageViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                _pageService.Update(pageViewModel);
                _pageService.Save();
                return Redirect("/admin/page/browse");
            }
        }

        #region AJAX API

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var model = await _pageService.GetAllAsync();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            var model = await _pageService.GetByIdAsync(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _pageService.Delete(id);
                _pageService.Save();
                return new OkObjectResult(id);
            }
        }

        [HttpPost]
        public IActionResult DeleteRange(List<int> idList)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else if (idList.Count == 0)
            {
                return new BadRequestObjectResult(ModelState);
            }
            else
            {
                _pageService.DeleteRange(idList);
                _pageService.Save();
                return new OkObjectResult(idList);
            }
        }

        #endregion AJAX API
    }
}