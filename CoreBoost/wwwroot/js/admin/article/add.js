class ArticleAdd {
    constructor() {
        this.init()
        this.registerEvents()
    }

    get _self() { return this }

    init() {
        const self = this._self

        $(() => {
            $('#Content').summernote({
                height: 300,
                maxHeight: 500,
                placeholder: "Bắt đầu viết...",
                callbacks: {
                    onBlur: function() {
                        $('#Content').html($('#Content').summernote('code'))
                    },
                    onImageUpload: function(files) {
                        let formData = new FormData()

                        formData.append("type", "articles")
                        formData.append("slug", $('#Slug').val())

                        for (var i = 0; i < files.length; i++) {
                            formData.append(files[0].name, files[0])
                        }

                        let path = ""

                        $.ajax({
                            type: "POST",
                            url: "/admin/upload/uploadsummernoteimage",
                            contentType: false,
                            processData: false,
                            data: formData,
                        })
                        .done((returnPath) => {
                            self.addNewImageNode(returnPath)
                        })
                        .fail((e) => {
                            new Noty({
                                type: 'error',
                                layout: 'bottomRight',
                                text: 'Upload ảnh thất bại.'
                            }).show()
                        })
                    },
                    onMediaDelete : function(target) {
                        console.log('xoá ảnh')
                    }
                }
            })
        })
    }

    registerEvents() {
        const self = this._self

        $('#imageSelect').on('change', function() {
            if ($('#Slug').val() == '') {
                $.alert({
                    theme: 'dark',
                    type: 'red',
                    boxWidth: '500px',
                    useBootstrap: false,
                    title: 'Chưa có slug cho ảnh!',
                    content: 'Bạn hãy nhập trường "Slug" trước rồi thử lại nhé.',
                })
            }
            else {
                // Check file name and file extension
                const fileUpload = $(this).get(0)
                const files = fileUpload.files
                let formData = new FormData()

                // Add slug key
                formData.append("type", "articles")
                formData.append("slug", $('#Slug').val())

                for (var i = 0; i < files.length; i++) {
                    formData.append(files[i].name, files[i])
                }

                self.uploadThumbnail(formData)
            }
        })

        $('#Title').on('change', function() {
            $('#Slug').val(self.generateSlug($(this).val()))
        })
    }

    // Features
    getSerializedChar(char) {
        switch (char) {
            case "á":
            case "à":
            case "ả":
            case "ã":
            case "ạ":
            case "ă":
            case "ắ":
            case "ằ":
            case "ẳ":
            case "ẵ":
            case "ặ":
            case "â":
            case "ấ":
            case "ầ":
            case "ẩ":
            case "ẫ":
            case "ậ":
                return "a"
                break
            case "đ":
                return "d"
                break
            case "é":
            case "è":
            case "ẻ":
            case "ẽ":
            case "ẹ":
            case "ê":
            case "ế":
            case "ề":
            case "ể":
            case "ễ":
            case "ệ":
                return "e"
                break
            case "í":
            case "ì":
            case "ỉ":
            case "ĩ":
            case "ị":
                return "i"
                break
            case "ó":
            case "ò":
            case "ỏ":
            case "õ":
            case "ọ":
            case "ô":
            case "ố":
            case "ồ":
            case "ổ":
            case "ỗ":
            case "ộ":
            case "ơ":
            case "ớ":
            case "ờ":
            case "ở":
            case "ỡ":
            case "ợ":
                return "o"
                break
            case "ú":
            case "ù":
            case "ủ":
            case "ũ":
            case "ụ":
            case "ư":
            case "ứ":
            case "ừ":
            case "ử":
            case "ữ":
            case "ự":
                return "u"
                break
            default:
                return char
        }        
    }

    generateSlug(title) {
        const self = this._self

        const wordArr = title.toLowerCase().split(' ')
        console.log(wordArr)
        const newWordArr = wordArr.map(word => {
            const charArr = word.split('')
            console.log(charArr)
            const serializedcharArr = charArr.map(char => self.getSerializedChar(char))
            charArr.map(char => console.log(`${char} => ${self.getSerializedChar(char)}`))
            return serializedcharArr.join('')
        })
        return newWordArr.join('-')
    }

    uploadThumbnail(formData) {
        $.ajax({
            type: "POST",
            url: "/admin/upload/uploadthumbnail",
            contentType: false,
            processData: false,
            data: formData,
        })
        .done((returnPath) => {
            $('#ImageUrl').val(returnPath)
            new Noty({
                type: 'success',
                layout: 'bottomRight',
                text: 'Upload ảnh thành công.'
            }).show()
        })
        .fail((e) => {
            new Noty({
                type: 'danger',
                layout: 'bottomRight',
                text: 'Upload ảnh thất bại.'
            }).show()
        })
    }

    uploadEditorImage(formData) {
        $.ajax({
            type: "POST",
            url: "/admin/upload/uploadsummernoteimage",
            contentType: false,
            processData: false,
            data: formData,
        })
        .done((returnPath) => {
            new Noty({
                type: 'success',
                layout: 'bottomRight',
                text: 'Upload ảnh thành công.'
            }).show()
        })
        .fail((e) => {
            new Noty({
                type: 'danger',
                layout: 'bottomRight',
                text: 'Upload ảnh thất bại.'
            }).show()
        })
    }

    addNewImageNode(path) {
        if (path == '' || path == undefined) {
            console.log(`Error: URL: ${path}`)

            new Noty({
                type: 'error',
                layout: 'bottomRight',
                text: 'Upload ảnh thất bại. Nguyên nhân: Đường dẫn trả về rỗng.'
            }).show()
        }
        else {
            let figureNode = document.createElement("figure")
            let imgNode = document.createElement("img")

            figureNode.classList.add("image")
            imgNode.src = path
            figureNode.appendChild(imgNode)

            $('#Content').summernote('insertNode', figureNode)

            new Noty({
                type: 'success',
                layout: 'bottomRight',
                text: 'Upload ảnh thành công.'
            }).show()
        }
    }
}