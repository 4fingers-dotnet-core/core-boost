let ve = {
    // Default
    configs: {
        global: {
            status: -1, // -1: both, 0: in-active, 1: active
            pageSize: 10,
            pageIndex: 1
        }
    },
    notify: (message, type) => {
        $.notify(message, {
            // whether to hide the notification on click
            clickToHide: true,
            // whether to auto-hide the notification
            autoHide: true,
            // if autoHide, hide after milliseconds
            autoHideDelay: 5000,
            // show the arrow pointing at the element
            arrowShow: true,
            // arrow size in pixels
            arrowSize: 5,
            // position defines the notification position though uses the defaults below
            position: '...',
            // default positions
            elementPosition: 'top right',
            globalPosition: 'top right',
            // default style
            style: 'bootstrap',
            // default class (string or [string])
            className: type,
            // show animation
            showAnimation: 'slideDown',
            // show animation duration
            showDuration: 400,
            // hide animation
            hideAnimation: 'slideUp',
            // hide animation duration
            hideDuration: 200,
            // padding between element and notification
            gap: 2
        })
    },
    confirm: (message, okCallback) => {
        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Hủy',
                    className: 'btn-danger'
                }
            },
            callback: (result) => {
                if (result === true) {
                    okCallback()
                }
            }
        })
    },
    dateFormatJson: (datetime) => {
        if (datetime == null || datetime == '')
            return ''
        let newdate = new Date(parseInt(datetime.substr(6)))
        let month = newdate.getMonth() + 1
        let day = newdate.getDate()
        let year = newdate.getFullYear()
        let hh = newdate.getHours()
        let mm = newdate.getMinutes()
        if (month < 10)
            month = "0" + month
        if (day < 10)
            day = "0" + day
        if (hh < 10)
            hh = "0" + hh
        if (mm < 10)
            mm = "0" + mm
        return day + "/" + month + "/" + year
    },
    dateFormat: (date) => {
        let dateString = date.substr(0, 10)
        let day = dateString.slice(8, 10)
        let month = dateString.slice(5, 7)
        let year = dateString.slice(0, 4)
        return day + "-" + month + "-" + year
    },
    dateTimeFormatJson: (datetime) => {
        if (datetime == null || datetime == '')
            return ''
        let newdate = new Date(parseInt(datetime.substr(6)))
        let month = newdate.getMonth() + 1
        let day = newdate.getDate()
        let year = newdate.getFullYear()
        let hh = newdate.getHours()
        let mm = newdate.getMinutes()
        let ss = newdate.getSeconds()
        if (month < 10)
            month = "0" + month
        if (day < 10)
            day = "0" + day
        if (hh < 10)
            hh = "0" + hh
        if (mm < 10)
            mm = "0" + mm
        if (ss < 10)
            ss = "0" + ss
        return day + "/" + month + "/" + year + " " + hh + ":" + mm + ":" + ss
    },
    startLoading: () => {
        if ($('.dv-loading').length > 0)
            $('.dv-loading').removeClass('hide')
    },
    stopLoading: () => {
        if ($('.dv-loading').length > 0)
            $('.dv-loading')
                .addClass('hide')
    },
    getFanpageStatusBadge: (status) => {
        return (status == 1) ? '<span class="tag is-warning">Đang Mở Bán</span>' : '<span class="tag is-success">Đã Bán</span>'
    },
    getTinTucStatus: (status) => {
        return (status == 1) ? '<span class="badge badge-pill badge-success">Đã duyệt</span>' : '<span class="badge badge-pill badge-danger">Chưa duyệt</span>'
    },
        
    getStatus: (status) => {
        return (status == 1) ? '<span class="badge badge-pill badge-success">Kích Hoạt</span>' : '<span class="badge badge-pill badge-danger">Bị Khoá</span>'
    },
    getEmployeeStatusBadge: (status) => {
        switch (status) {
            case 0:
                return getEmployeeStatusMarkUp("secondary", "Chưa PV")
                break;
            case 1:
                return getEmployeeStatusMarkUp("info", "Đạt PV 1")
                break;
            case 2:
                return getEmployeeStatusMarkUp("primary", "Thử Việc 1")
                break;
            case 3:
                return getEmployeeStatusMarkUp("info", "Đạt PV 2")
                break;
            case 4:
                return getEmployeeStatusMarkUp("primary", "Thử Việc 2")
                break;
            case 5:
                return getEmployeeStatusMarkUp("success", "Đang Làm Việc")
                break;
            case 6:
                return getEmployeeStatusMarkUp("warning", "Nghỉ Tạm Thời")
                break;
            case 7:
                return getEmployeeStatusMarkUp("secondary", "Nghỉ Việc")
                break;
            default:
                return getEmployeeStatusMarkUp("danger", "Status not vaild!")
        }
    },
    formatNumber: (number, precision) => {
        if (!isFinite(number)) {
            return number.toString()
        }

        let a = number.toFixed(precision).split('.')
        a[0] = a[0].replace(/\d(?=(\d{3})+$)/g, '$&,')
        return a.join('.')
    },
    unflattern: (arr) => {
        let map = {}
        let roots = []
        for (let i = 0; i < arr.length; i += 1) {
            let node = arr[i]
            node.children = []
            map[node.id] = i // use map to look-up the parents
            if (node.parentId !== null) {
                arr[map[node.parentId]].children.push(node)
            } else {
                roots.push(node)
            }
        }
        return roots
    }
}

$(document).ajaxSend(function (e, xhr, options) {
    if (options.type.toUpperCase() == "POST" || options.type.toUpperCase() == "PUT") {
        let token = $('form').find("input[name='__RequestVerificationToken']").val()
        xhr.setRequestHeader("RequestVerificationToken", token)
    }
})