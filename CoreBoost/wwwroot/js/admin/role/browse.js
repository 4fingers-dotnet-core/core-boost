class RoleBrowse {
    constructor() {
        this.init()
        this.registerEvents()
    }

    get _self() { return this }

    init() {
        // Select current pageSize base on URL
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("pageSize"))
            $('#pageSizeSelect').val(params.get("pageSize"))

        if (params.has("keyword"))
            $('#keyword').val(params.get("keyword"))
    }

    registerEvents() {
        const self = this._self

        $('#pageSizeSelect').on('change', function () {
            self.changePageSize($(this).val())
        })

        $('#searchButton').on('click', function () {
            self.search($('#keyword').val())
        })

        $('body').on('click', '.delete-button', function (e) {
            var roleId = $(this).data('id')

            $.confirm({
                theme: 'dark',
                type: 'red',
                title: 'Bạn chắc chắn muốn xóa chứ?',
                content: 'Thao tác đã thực hiện sẽ không thể khôi phục lại.',
                buttons: {
                    confirm: {
                        text: 'Đồng Ý',
                        action: () => {
                            self.delete(roleId);
                        }
                    },
                    cancel: {
                        text: "Hủy Bỏ"
                    }
                }
            })
        })
    }

    search(keyword) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("keyword"))
            params.set("keyword", keyword)
        else
            params.append("keyword", keyword)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    changePageSize(number) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("pageSize"))
            params.set("pageSize", number)
        else
            params.append("pageSize", number)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    goToPage(index) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("page"))
            params.set("page", index)
        else
            params.append("page", index)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    delete(roleId) {
        $.ajax({
            type: "POST",
            url: "/admin/role/delete",
            data: { id: roleId },
        })
        .done(() => {
            new Noty({
                type: 'info',
                layout: 'bottomRight',
                text: 'Xoá thành công, trang sẽ được tải lại sau 3s.'
            }).show()
            setTimeout(() => location.reload(true), 3000)
        })
        .fail(() => {
            console.log('Lỗi xảy ra khi xoá')
        })
    }
}