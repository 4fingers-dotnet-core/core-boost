class ArticleCategoryBrowse {
    constructor() {
        this.init()
        this.registerEvents()
    }

    get _self() { return this }

    init() {
        // Remove checked when reload
        $('#selectAllCheck').prop('checked', false)
        $.each($('.admin__table__checkbox:checked'), function () {
            $(this).prop('checked', false)
        })

        // Select current pageSize base on URL
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("pageSize"))
            $('#pageSizeSelect').val(params.get("pageSize"))

        if (params.has("keyword"))
            $('#keyword').val(params.get("keyword"))
    }

    registerEvents() {
        const self = this._self

        $('#applySelected').on('click', function() {
            let idArr = []

            if ($("#bulkAction").val() == "none") {
                $.alert({
                    theme: 'dark',
                    type: 'red',
                    boxWidth: '500px',
                    useBootstrap: false,
                    title: 'Chưa chọn hành động cụ thể!',
                    content: 'Bạn hãy chọn một mục từ menu dropdown bên cạnh nhé.',
                });
            }
            
            if ($("#bulkAction").val() == "delete") {
                $.each($('.admin__table__checkbox:checked'), function () {
                    idArr.push(parseInt($(this).val()))
                })
                console.log(idArr)

                if (idArr.length > 0) {
                    $.confirm({
                        theme: 'dark',
                        type: 'red',
                        title: 'Bạn chắc chắn muốn xóa những mục đã chọn chứ?',
                        content: 'Thao tác đã thực hiện sẽ không thể khôi phục lại.',
                        buttons: {
                            confirm: {
                                text: 'Đồng Ý',
                                action: () => {
                                    self.deleteRange(idArr)
                                }
                            },
                            cancel: {
                                text: "Hủy Bỏ"
                            }
                        }
                    })
                }
                else {
                    $.alert({
                        theme: 'dark',
                        type: 'red',
                        boxWidth: '500px',
                        useBootstrap: false,
                        title: 'Chưa đánh dấu mục cần xoá!',
                        content: 'Bạn hãy chọn ít nhât một mục tại checkbox tương ứng nhé.',
                    });
                }
                
            }
        })

        $('#pageSizeSelect').on('change', function () {
            self.changePageSize($(this).val())
        })

        $('#searchButton').on('click', function () {
            self.search($('#keyword').val())
        })

        $("#selectAllCheck").click(function() {
            $('.admin__table__checkbox').not(this).prop('checked', this.checked)
        })

        $('body').on('click', '.delete-button', function (e) {
            var articleCategoryId = $(this).data('id')

            $.confirm({
                theme: 'dark',
                type: 'red',
                title: 'Bạn chắc chắn muốn xóa chứ?',
                content: 'Thao tác đã thực hiện sẽ không thể khôi phục lại.',
                buttons: {
                    confirm: {
                        text: 'Đồng Ý',
                        action: () => {
                            self.delete(articleCategoryId);
                        }
                    },
                    cancel: {
                        text: "Hủy Bỏ"
                    }
                }
            })
        })
    }


    search(keyword) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("keyword"))
            params.set("keyword", keyword)
        else
            params.append("keyword", keyword)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    changePageSize(number) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("pageSize"))
            params.set("pageSize", number)
        else
            params.append("pageSize", number)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    goToPage(index) {
        let url = new URL(window.location.href)
        let params = new URLSearchParams(url.search.slice(1))

        if (params.has("page"))
            params.set("page", index)
        else
            params.append("page", index)

        url.search = params.toString()
        window.location.href = url.toString()
    }

    delete(articleCategoryId) {
        $.ajax({
            type: "POST",
            url: "/admin/articlecategory/delete",
            data: { id: articleCategoryId },
        })
        .done(() => {
            new Noty({
                type: 'info',
                layout: 'bottomRight',
                text: 'Xoá thành công, trang sẽ được tải lại sau 3s.'
            }).show();
            setTimeout(() => location.reload(true), 3000)
        })
        .fail(() => {
            console.log('Lỗi xảy ra khi xoá')
        })
    }

    deleteRange(idList) {
        $.ajax({
            type: "POST",
            url: "/admin/articlecategory/deleterange",
            data: { idList: idList },
        })
        .done(() => {
            new Noty({
                type: 'info',
                layout: 'bottomRight',
                text: 'Xoá hàng loạt thành công, trang sẽ được tải lại sau 3s.'
            }).show();
            setTimeout(() => location.reload(true), 3000)
        })
        .fail(() => {
            console.log('Lỗi xảy ra khi xoá hàng loạt!')
        })
    }
}