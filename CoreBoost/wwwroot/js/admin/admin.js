const openNav = () => {
    document.getElementById("sidenav").style.width = "250px"
    document.getElementById("main").style.marginLeft = "250px"
}

const closeNav = () => {
    document.getElementById("sidenav").style.width = "0"
    document.getElementById("main").style.marginLeft = "0"
}

const toggleNav = () => {
    if ($('#sidenav').width() > 0) {
        closeNav()
        $('#topbar-btn').html("<i class=\"fas fa-bars\"></i>")
    }
    else {
        openNav()
        $('#topbar-btn').html("<i class=\"fas fa-arrow-left\"></i>")
    }
    
}

const toggleActive = (element) => {
    const e = $(element)

    if (!e.hasClass("active")) {
        e.addClass("active")
        e.next().css("display", "block")
    }
    else {
        e.removeClass("active")
        e.next().css("display", "none")
    }
}

const toggleTopbarDropdown = (element) => {
    // const dropdown = $(element).parent().parent().parent().parent().parent()
    const e = $(element)
    !e.hasClass("is-active") ? e.addClass("is-active") : e.removeClass("is-active")
}