﻿using AutoMapper;
using CoreBoost.Application.AutoMapper;
using CoreBoost.Application.Dapper.Implementation;
using CoreBoost.Application.Dapper.Interfaces;
using CoreBoost.Application.Implementation;
using CoreBoost.Application.Interfaces;
using CoreBoost.Authorization;
using CoreBoost.Data.EF;
using CoreBoost.Data.EF.Repositories;
using CoreBoost.Data.Entities;
using CoreBoost.Data.IRepositories;
using CoreBoost.Extensions;
using CoreBoost.Helpers;
using CoreBoost.Infrastructure.Interfaces;
using CoreBoost.Services;
using CoreBoost.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using PaulMiami.AspNetCore.Mvc.Recaptcha;
using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace CoreBoost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string AllowSpecificOrigins = "_allowSpecificOrigins";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Specify the HTTPS port 
            // services.AddHttpsRedirection(options =>
            // {
            //     options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
            //     options.HttpsPort = 443;
            // });
            // 3.0
            // If using Kestrel:
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddControllersWithViews()
                // Razor runtime compilation
                .AddRazorRuntimeCompilation();
                
            services.AddRazorPages();
            
            // Use SQlite for development, use SQLServer for production
            // services.AddDbContext<CoreBoostContext>(options => {
            //     options.UseSqlServer(Configuration.GetConnectionString("SQLServerConnection"),
            //         o => o.MigrationsAssembly("CoreBoost.Data.EF")
            //     );
            //     options.UseLazyLoadingProxies();
            // });
            // if(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            //     services.AddDbContext<CoreBoostContext>(options =>
            //         options.UseSqlServer(Configuration.GetConnectionString("SQLServerConnection")));
            // else        
            services.AddDbContext<CoreBoostContext>(options => {
                options.UseSqlite(
                    Configuration.GetConnectionString("SQLiteConnection")
                );
                options.UseLazyLoadingProxies();
            });

            // services.AddDbContext<CoreBoostContext>(options =>
            //     options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
            //     o => o.MigrationsAssembly("CoreBoost.Data.EF")));

            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<CoreBoostContext>()
                .AddDefaultTokenProviders();

            services.AddMemoryCache();

            services.AddMinResponse();

            services.AddAutoMapper(typeof(Startup).Assembly);
            services.AddSingleton<AutoMapper.IConfigurationProvider>(AutoMapperConfig.RegisterMappings());
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));

            // Configure Identity
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            // Test
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);

                options.LoginPath = "/Admin/Account/Login";
                options.AccessDeniedPath = "/Admin/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            services.AddRecaptcha(new RecaptchaOptions()
            {
                SiteKey = Configuration["Recaptcha:SiteKey"],
                SecretKey = Configuration["Recaptcha:SecretKey"]
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(2);
                options.Cookie.HttpOnly = true;
            });
            services.AddImageResizer();
            // services.AddAuthentication()
            //     .AddFacebook(facebookOpts =>
            //     {
            //         facebookOpts.AppId = Configuration["Authentication:Facebook:AppId"];
            //         facebookOpts.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            //     })
            //     .AddGoogle(googleOpts =>
            //     {
            //         googleOpts.ClientId = Configuration["Authentication:Google:ClientId"];
            //         googleOpts.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            //     });
            // Add application services.
            services.AddScoped<UserManager<AppUser>, UserManager<AppUser>>();
            services.AddScoped<RoleManager<AppRole>, RoleManager<AppRole>>();

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IViewRenderService, ViewRenderService>();

            services.AddTransient<DbInitializer>();

            services.AddScoped<IUserClaimsPrincipalFactory<AppUser>, CustomClaimsPrincipalFactory>();

            // services.AddMvc(options =>
            // {
            //     options.CacheProfiles.Add("Default",
            //         new CacheProfile()
            //         {
            //             Duration = 60
            //         });
            //     options.CacheProfiles.Add("Never",
            //         new CacheProfile()
            //         {
            //             Location = ResponseCacheLocation.None,
            //             NoStore = true
            //         });
            // }).AddViewLocalization(
            //         LanguageViewLocationExpanderFormat.Suffix,
            //         opts => { opts.ResourcesPath = "Resources"; })
            //     .AddDataAnnotationsLocalization();
            //     //.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            services.AddCors(options => options.AddPolicy(AllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:54151")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                }));

            services.Configure<RequestLocalizationOptions>(
              opts =>
              {
                  var supportedCultures = new List<CultureInfo>
                  {
                        new CultureInfo("en-US"),
                        new CultureInfo("vi-VN")
                  };

                  opts.DefaultRequestCulture = new RequestCulture("en-US");
                  // Formatting numbers, dates, etc.
                  opts.SupportedCultures = supportedCultures;
                  // UI strings that we have localized.
                  opts.SupportedUICultures = supportedCultures;
              });

            // Repository
            services.AddTransient(typeof(IUnitOfWork), typeof(EFUnitOfWork));
            services.AddTransient(typeof(IRepository<,>), typeof(EFRepository<,>));

            // Custom repository for CoreBoost
            services.AddTransient<IArticleRepository, ArticleRepository>();
            services.AddTransient<IArticleCategoryRepository, ArticleCategoryRepository>();
            services.AddTransient<IFeedbackRepository, FeedbackRepository>();
            services.AddTransient<IPageRepository, PageRepository>();

            // Services
            services.AddTransient<IFunctionService, FunctionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IContactService, ContactService>();
            services.AddTransient<IPageService, PageService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IAuthorizationHandler, BaseResourceAuthorizationHandler>();

            // Custom services for CoreBoost
            services.AddTransient<IArticleService, ArticleService>();
            services.AddTransient<IArticleCategoryService, ArticleCategoryService>();
            services.AddTransient<IPageService, PageService>();

            // SignalR
            services.AddSignalR();
            services.AddHttpClient();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/CoreBoost-{Date}.txt");
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            // NWebsec
            app.UseXContentTypeOptions();
            app.UseReferrerPolicy(opts => opts.NoReferrer());
            app.UseXXssProtection(options => options.EnabledWithBlockMode());
            app.UseXfo(options => options.Deny());
            app.UseCsp(opts => opts
                .BlockAllMixedContent()
                //.StyleSources(s => s.Self())
                //.StyleSources(s => s.UnsafeInline())
                .FontSources(s => s.Self().CustomSources("fonts.gstatic.com"))
                .FormActions(s => s.Self())
                .FrameAncestors(s => s.Self())
                .ImageSources(s => s.Self().CustomSources("via.placeholder.com", "graph.facebook.com", "scontent.xx.fbcdn.net"))
                //.ScriptSources(s => s.Self())
            );

            // This middleware must be placed before UseStaticFiles()
            app.UseImageResizer();
            app.UseStaticFiles();
            app.UseMinResponse();
            
            app.UseSession();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            
            // 3.0
            app.UseRouting();
            app.UseCors(AllowSpecificOrigins);
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapHub<TeduHub>("/teduHub");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("areaRoute", "{area:exists}/{controller=Login}/{action=Index}/{id?}");
            });
        }
    }
}