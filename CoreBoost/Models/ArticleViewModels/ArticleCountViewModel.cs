namespace CoreBoost.Models.ArticleViewModels
{
    public class ArticleCountViewModel
    {
        public int All { get; set; }
        public int Active { get; set; }
        public int InActive { get; set; }
    }
}