﻿using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
