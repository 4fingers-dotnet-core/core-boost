﻿using System.ComponentModel.DataAnnotations;

namespace CoreBoost.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Yêu cầu tên tài khoản")]
        [Display(Name = "Tài Khoản")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Yêu cầu mật khẩu.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật Khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ tôi")]
        public bool RememberMe { get; set; }
    }
}
