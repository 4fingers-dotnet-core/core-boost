﻿using System.Collections.Generic;
using CoreBoost.Application.ViewModels.Article;

namespace CoreBoost.Models
{
    public class HomeViewModel
    {
        public string Title { set; get; }
        public string MetaKeyword { set; get; }
        public string MetaDescription { set; get; }
        public List<ArticleViewModel> Articles { get; set; }
    }
}
