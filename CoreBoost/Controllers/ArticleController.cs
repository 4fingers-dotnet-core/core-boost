using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreBoost.Application.Interfaces;

namespace CoreBoost.Controllers
{
    [Route("article")]
    public class ArticleController : Controller
    {
        private IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [Route("all")]
        public async Task<IActionResult> All(int? categoryId, string keyword, int? status, int? page, int? pageSize)
        {
            var articles = await _articleService.GetAllPagingAsync(categoryId, keyword, status, page, pageSize);

            ViewData["Title"] = "Tất Cả Tin Tức";
            ViewData["Description"] = "Let's hype your bussiness!";
            ViewData["Keywords"] = "venthusiam, agency, marketing, phần mềm, tư vấn";

            return View(articles);
        }

        [Route("{slug}")]
        public async Task<IActionResult> Detail(string slug)
        {
            var article = await _articleService.GetBySlugAsync(slug);

            ViewData["Title"] = article.Title;
            ViewData["Description"] = article.Excerpt;
            ViewData["Keywords"] = article.Keywords;

            return View(article);
        }
    }
}