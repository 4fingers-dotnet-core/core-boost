﻿using CoreBoost.Application.Interfaces;
using CoreBoost.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using CoreBoost.Application.ViewModels.Common;
using CoreBoost.Data.Enums;

namespace CoreBoost.Controllers
{
    public class HomeController : Controller
    {
        private IArticleService _articleService;
        private readonly IHttpClientFactory _httpClientFactory;

        public HomeController(IArticleService articleService, IHttpClientFactory httpClientFactory)
        {
            _articleService = articleService;
            _httpClientFactory = httpClientFactory;
        }

        #region AJAX
        public async Task<bool> ReCaptchaPassed(string gRecaptchaResponse)
        {
            var requestUrl = $"https://www.google.com/recaptcha/api/siteverify?secret=6Lcw0N8UAAAAAOeVpsJ7YX2A4YZm4qPRJuEbjzf6&response={gRecaptchaResponse}";
            var request = new HttpRequestMessage(HttpMethod.Post, requestUrl);
            var client = _httpClientFactory.CreateClient();
            var response = await client.SendAsync(request);

            return response.IsSuccessStatusCode;
        }

        [HttpPost]
        public async Task<IActionResult> SendResponse()
        {
            if (ModelState.IsValid)
                if (!await ReCaptchaPassed(Request.Form["token"]))
                {
                    ModelState.AddModelError(string.Empty, "You failed the CAPTCHA.");
                    return new BadRequestObjectResult(ModelState);
                }
                else
                {
                    // var feedback = new FeedbackViewModel();
                    // feedback.Name = Request.Form["name"];
                    // feedback.Email = Request.Form["email"];
                    // feedback.Phone = Request.Form["phone"];
                    // feedback.Message = Request.Form["message"];
                    // feedback.Status = Status.Active;
                    // feedback.DateCreated = DateTime.Now;
                    // feedback.DateModified = DateTime.Now;

                    // _feedbackService.Add(feedback);

                    return new OkResult();
                }
                    
            else
            {
                ModelState.AddModelError(string.Empty, "Form data wrong.");
                return new BadRequestObjectResult(ModelState);
            }
        }

        #endregion

        //[ResponseCache(CacheProfileName = "Default")]
        public async Task<IActionResult> Index()
        {
            ViewData["Title"] = "CoreBoost";
            ViewData["Description"] = "Starter .NET Core template";
            ViewData["Keywords"] = "coreboost, template";

            var homeViewModel = new HomeViewModel();
            homeViewModel.Articles = await _articleService.GetAllAsync();
            
            return View(homeViewModel);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
