using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using CoreBoost.Application.Interfaces;
using CoreBoost.Data.EF;

namespace VEenthusiam.Controllers.Components
{
    public class RecommendedArticlesViewComponent : ViewComponent
    {
        private IArticleService _articleService;

        public RecommendedArticlesViewComponent(IArticleService articleService)
        {
            _articleService = articleService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var articles = await _articleService.GetLastestAsync(3);
            return View(articles);
        }
    }
}