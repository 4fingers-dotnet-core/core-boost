﻿using CoreBoost.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreBoost.Controllers
{
    [Route("page")]
    public class PageController : Controller
    {
        private IPageService _pageService;

        public PageController(IPageService pageService)
        {
            _pageService = pageService;
        }

        [Route("all")]
        public async Task<IActionResult> All(string keyword, int? status, int? page, int? pageSize)
        {
            var pages = await _pageService.GetAllPagingAsync(keyword, status, page, pageSize);

            ViewData["Title"] = "Tất Cả Trang";
            ViewData["Description"] = "Let's hype your bussiness!";
            ViewData["Keywords"] = "venthusiam, agency, marketing, phần mềm, tư vấn";

            return View(pages);
        }

        [Route("{slug}")]
        public async Task<IActionResult> Detail(string slug)
        {
            var page = await _pageService.GetBySlugAsync(slug);

            ViewData["Title"] = page.Title;
            ViewData["Description"] = page.Excerpt;
            ViewData["Keywords"] = page.Keywords;

            return View(page);
        }

        [Route("policy")]
        public IActionResult Policy()
        {
            ViewData["Title"] = "Nguyên tắc về quyền riêng tư của CoreBoost";
            ViewData["Description"] = "Nguyên tắc về quyền riêng tư của CoreBoost";
            ViewData["Keywords"] = "venthusiam, policy";

            return View();
            
        }

        public IActionResult FAQ()
        {
            return View();
        }
    }
}