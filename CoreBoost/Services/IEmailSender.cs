﻿using System.Threading.Tasks;

namespace CoreBoost.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
